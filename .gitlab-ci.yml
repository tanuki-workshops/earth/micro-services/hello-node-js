stages:
  - 🐳build
  - 🚢deploy

variables:
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80

#----------------------------
# Build Docker image
#----------------------------
📦kaniko-build:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: 🐳build
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script: |
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    # 👋 Don't forget to use CI_COMMIT_SHORT_SHA instead CI_COMMIT_TAG (it's empty, because it's present only when building tag)

#----------------------------
# YAML Anchors
#----------------------------
.generate-manifest: &generate-manifest
- |
  envsubst < ./kube/deploy.template.yaml > ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml
  cat -n ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml

.apply: &apply
- |
  kubectl apply -f ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml -n ${KUBE_NAMESPACE}

.delete: &delete
- |
  kubectl delete -f ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml -n ${KUBE_NAMESPACE}
  # TODO: delete KUBE_NAMESPACE
.scale-by-3: &scale-by-3
- |
  kubectl scale --replicas=3 deploy ${CI_PROJECT_NAME} -n ${KUBE_NAMESPACE}

.scale-by-1: &scale-by-1
- |
  kubectl scale --replicas=1 deploy ${CI_PROJECT_NAME} -n ${KUBE_NAMESPACE}

#----------------------------
# Deploy
#----------------------------
🚀production-deploy:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/earth/images/ci-tools/kube
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  environment:
    name: production/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
  script:
    - *generate-manifest
    - *apply
    #- *scale-by-3
    - *scale-by-1

🎉preview-deploy:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/earth/images/ci-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 😢stop-preview-deploy
  script:
    - *generate-manifest
    - *apply
    - *scale-by-1

😢stop-preview-deploy:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/earth/images/ci-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    action: stop
  script:
    - *generate-manifest
    - *delete
